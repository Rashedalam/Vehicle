<?php
include_once 'vendor/autoload.php';

use Pondit\Vehicle\LandVehicle\Car;
use Pondit\Vehicle\LandVehicle\Truck;
use Pondit\Vehicle\WaterVehicle\Ship;
use Pondit\Vehicle\AirVehicle\Plane;
use Pondit\Vehicle\AirVehicle\Helicopter;

$car1 = new Car();
var_dump($car1);

$truck = new Truck();
var_dump($truck);

$ship=new Ship();
var_dump($ship);

$plane=new Plane();
var_dump($plane);

$helicopter=new Helicopter();
var_dump($helicopter);